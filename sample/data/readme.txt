By Jung Min-ho

Ren Zhengfei, the founder and CEO of Chinese tech giant Huawei, called U.S. President Donald Trump "a great president" as his company struggles with spying allegations by the United States and some of its allies.

Speaking to journalists from Western media at Huawei's headquarters in Shenzhen, Tuesday, Ren, 74, also said he would never allow China's government to access customer information, in a bid to assuage growing global security concerns about his company.

"Trump is a great president," Ren reportedly said. "He dares to massively cut taxes, which will benefit business. But you have to treat well the companies and countries so that they are willing to invest in the U.S. and the government will be able to collect enough tax."

The emergence of the reclusive leader, who last spoke with foreign media nearly four years ago, underlines the magnitude of the issue facing Huawei, the symbol of China's tech might.

So far, the U.S., Australia, New Zealand and Japan have barred Huawei from taking part in their 5G infrastructure build-outs amid concerns of its links with China's government. More countries are now considering the same move.

Ren's background �� a former engineer at the People's Liberation Army and a current party member �� did not help resolve the doubts.

"I love my country, I support the Communist Party," Ren said. "But I will not do anything to harm the world. I don't see a close connection between my personal political beliefs and the businesses of Huawei."

But critics say Huawei would have no choice but to comply with Chinese intelligence officials by law, if they made such a request.

Ren nevertheless promised he would always be "sided with customers."

"The values of a business entity is customer first, is customer centricity," he said. "We are a business organization, so we must follow business rules �� And I think I already made myself very clear right now, we will definitely say no to such a request."

Meng Wanzhou, a senior Huawei executive and Ren's daughter, has been arrested in Vancouver at the request of U.S. authorities. She is suspected of fraud related to payments that violated U.S. sanctions against Iran.

When asked about her, Ren said he misses her.

"As her father, I miss her very much �� I trust that the legal systems of Canada and the United States are open, just and fair, and will reach a just conclusion," he said. "We will make our judgment after all the evidence is made public."