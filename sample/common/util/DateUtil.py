'''
Created on 2019. 1. 17.

@author: Andy
'''

import datetime 

class DateUtils(object):
    
    timeToStartWork = 9
    timeToGetOffWork = 18
    
    """
    Date 처리 관련 utitlity class입니다.
    """
    
    @staticmethod
    def createDateTime(date:'날짜 dateimte.date', time:'시간 datetime.time'):
        """    
        날짜와 시간을 합쳐서 datetime을 되돌린다.
        """        
        return datetime.datetime.combine(date,time)
    
    @staticmethod
    def getHours(time:'시간계산값 timedelta'):
        """
        다 날짜의 시간차이를 계산하여 반환한다.
        """
        return time.seconds / 3600
    
    @staticmethod
    def getOvertimeHours(time:'시간계산값 timedelta'):
        '''
        초과 근무시간을 계산하여 반환한다.
        
        '''
        workHours = time.seconds / 3600
        if workHours <=8:
            return 0
        else:
            return workHours - 8
        
    @staticmethod
    def getExtraHours(time:'datetime.time 객체'):
        if time.hour < 18:
            return 0
        else:
            return time.hour - 18
