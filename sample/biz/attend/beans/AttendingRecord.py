'''
Created on 2019. 1. 17.

@author: Andy
'''
from framework.base.BaseBean import BaseBean


class AttendingRecord(BaseBean):
    """
    출퇴근 기록
    """
    
    def __init__(self):
        
        self.userId = None #사용자 아이디
        self.overHours = 0 #초과근무시간
        self.attendDate = None #출근날짜
        self.dtToAttend = None #출근시간 
        self.dtToGetOff = None #퇴근시간
        





