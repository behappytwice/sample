# -*- coding: utf-8 -*-
from biz.attend.beans.AttendingRecord import *
import datetime 
from common.util.DateUtil import DateUtils

# record = AttendingRecord()
# record.userId = "kim"
# record.attendDate = datetime.datetime.now()
# record.dtToAttend = DateUtils.createDateTime(datetime.date(2018,1,5), datetime.time(3,0,0))
# record.dtToGetOff = DateUtils.createDateTime(datetime.date(2018,1,5), datetime.time(5,0,0))
# result = record.dtToGetOff - record.dtToAttend
# record.overHours = DateUtils.getOvertimeHours(result)
# print('초과근무시간: {0}'.format(record.overHours))
# print(record.userId)
# print(result.seconds)
# print(type(result))
# print(DateUtils.getHours(result))
#
f = open("data/attendrecords.txt")
extraHours = []
for line in f.readlines():
    #print(line)
    splitedLine = line.split(",")
    splitedTime1 = splitedLine[0].split(':') #출근시간
    splitedTime2 = splitedLine[1].split(':') #퇴근시간
    time1 = datetime.time(int(splitedTime1[0].strip()), int(splitedTime1[1].strip()),0)
    time2 = datetime.time(int(splitedTime2[0].strip()), int(splitedTime2[1].strip()),0)
    print(time2)
    extraHours.append(DateUtils.getExtraHours(time2))
f.close()
# 여기서부터 초과근무시간을 구하는 로직 
sum = 0
for et in extraHours:
    sum = sum + et
          
everage = sum /len(extraHours)

print("평균 초과근무시간은 {0}입니다.".format(everage))


"""
초과 근무시간은 18시를 넘으면 
초과시간 = (시간 - 18) * 60 + 분 
초과근무시간 = 초과시간 / 60
나머지는 초과분 
평균 분을 구한 다음에 그것을 60으로 나누어 초과근무시간으로 표시
"""

